package org.example.shape;

import lombok.Getter;
import lombok.Setter;
import org.example.visitor.Visitor;

/**
 * LineSegment.
 *
 * @author Tatyana_Dolnikova
 */
@Getter
@Setter
public class LineSegment extends Shape {

    private int width;

    public LineSegment(int width) {
        this.width = width;
        name = "ОТРЕЗОК";
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
