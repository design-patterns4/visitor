package org.example.shape;

import lombok.Getter;
import lombok.Setter;
import org.example.visitor.Visitor;

/**
 * Square.
 *
 * @author Tatyana_Dolnikova
 */
@Getter
@Setter
public class Square extends Shape {

    private int side;

    public Square(int side) {
        this.side = side;
        name = "КВАДРАТ";
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
