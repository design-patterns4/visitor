package org.example.shape;

import lombok.Getter;
import lombok.Setter;
import org.example.visitor.Visitor;

/**
 * Circle.
 *
 * @author Tatyana_Dolnikova
 */
@Getter
@Setter
public class Rhombus extends Shape {

    private int diagonal1;
    private int diagonal2;
    private int side;

    public Rhombus(int diagonal1, int diagonal2, int side) {
        this.diagonal1 = diagonal1;
        this.diagonal2 = diagonal2;
        this.side = side;
        name = "РОМБ";
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
