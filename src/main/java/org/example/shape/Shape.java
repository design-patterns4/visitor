package org.example.shape;

import org.example.visitor.Visitor;

/**
 * Shape.
 *
 * @author Tatyana_Dolnikova
 */
public abstract class Shape {

    public String name;

    public abstract void accept(Visitor visitor);

    public void printName() {
        System.out.println(name);
    }
}
