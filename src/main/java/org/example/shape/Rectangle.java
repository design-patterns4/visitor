package org.example.shape;

import lombok.Getter;
import lombok.Setter;
import org.example.visitor.Visitor;

/**
 * Rectangle.
 *
 * @author Tatyana_Dolnikova
 */
@Getter
@Setter
public class Rectangle extends Shape {

    private int side1;
    private int side2;

    public Rectangle(int side1, int side2) {
        this.side1 = side1;
        this.side2 = side2;
        name = "ПРЯМОУГОЛЬНИК";
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

}