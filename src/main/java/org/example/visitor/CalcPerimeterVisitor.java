package org.example.visitor;

import org.example.shape.LineSegment;
import org.example.shape.Rectangle;
import org.example.shape.Rhombus;
import org.example.shape.Square;

/**
 * CalcPerimeterVisitor.
 *
 * @author Tatyana_Dolnikova
 */
public class CalcPerimeterVisitor extends Visitor {

    @Override
    public void visit(Rhombus rhombus) {
        System.out.print("Периметр ромба: ");
        System.out.println((float) 4 * rhombus.getSide());
    }

    @Override
    public void visit(Square square) {
        System.out.print("Периметр квадрата: ");
        System.out.println((float) 4 * square.getSide());
    }

    @Override
    public void visit(Rectangle rectangle) {
        System.out.print("Периметр прямоугольника: ");
        System.out.println((float) rectangle.getSide1() * 2 + rectangle.getSide2() * 2);
    }

    @Override
    public void visit(LineSegment lineSegment) {
        System.out.print("Периметр отрезка: ");
        System.out.println("Нет");
    }
}
