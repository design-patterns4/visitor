package org.example.visitor;

import org.example.shape.LineSegment;
import org.example.shape.Rectangle;
import org.example.shape.Rhombus;
import org.example.shape.Square;

/**
 * DrawingVisitor.
 *
 * @author Tatyana_Dolnikova
 */
public class DrawingVisitor extends Visitor {

    @Override
    public void visit(Rhombus rhombus) {
        int diagonal1 = rhombus.getDiagonal1();
        int z = 1;
        for (int i = 0; i < diagonal1 / 2 + 1; i++) {
            int prob = (diagonal1 - z) / 2;

            for (int j = 0; j < prob; j++) {
                System.out.print(" ");
            }
            for (int k = 0; k < z; k++) {
                System.out.print("*");
            }
            System.out.println();

            z = z + 2;
        }
        z = diagonal1 - 2;

        for (int i = diagonal1 / 2; i > 0; i--) {
            int prob = (diagonal1 - z) / 2;
            for (int j = 0; j < prob; j++) {
                System.out.print(" ");
            }
            for (int k = 0; k < z; k++) {
                System.out.print("*");
            }
            System.out.println();
            z = z - 2;
        }
    }

    @Override
    public void visit(Square square) {
        int side = square.getSide();
        for (int i = 0; i < side; i++) {
            for (int j = 0; j < side; j++) {
                System.out.print("o");
            }
            System.out.println("o");
        }
    }

    @Override
    public void visit(Rectangle rectangle) {
        int side1 = rectangle.getSide1();
        int side2 = rectangle.getSide2();
        for (int i = 0; i < side1; i++) {
            for (int j = 0; j < side2; j++) {
                System.out.print("#");
            }
            System.out.println("#");
        }
    }

    @Override
    public void visit(LineSegment lineSegment) {
        int width = lineSegment.getWidth();
        for (int i = 0; i < width; i++) {
            System.out.print("_");
        }
        System.out.println();
    }

}
