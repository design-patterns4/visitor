package org.example.visitor;

import org.example.shape.LineSegment;
import org.example.shape.Rectangle;
import org.example.shape.Rhombus;
import org.example.shape.Square;

/**
 * CalcPerimeterVisitor.
 *
 * @author Tatyana_Dolnikova
 */
public class CalcAreaVisitor extends Visitor {

    @Override
    public void visit(Rhombus rhombus) {
        System.out.print("Площадь ромба: ");
        System.out.println((float) (rhombus.getDiagonal1() * rhombus.getDiagonal2() / 2));
    }

    @Override
    public void visit(Square square) {
        System.out.print("Площадь квадрата: ");
        System.out.println(square.getSide() * square.getSide());
    }

    @Override
    public void visit(Rectangle rectangle) {
        System.out.print("Площадь прямоугольника: ");
        System.out.println(rectangle.getSide1() * rectangle.getSide2());
    }

    @Override
    public void visit(LineSegment lineSegment) {
        System.out.print("Площадь отрезка: ");
        System.out.println("Нет");
    }
}
