package org.example.visitor;

import org.example.shape.LineSegment;
import org.example.shape.Rectangle;
import org.example.shape.Rhombus;
import org.example.shape.Square;

/**
 * Visitor.
 *
 * @author Tatyana_Dolnikova
 */
public abstract class Visitor {

    public abstract void visit(Rhombus rhombus);

    public abstract void visit(Square square);

    public abstract void visit(Rectangle rectangle);

    public abstract void visit(LineSegment lineSegment);

}
