package org.example;

import lombok.var;
import org.example.shape.LineSegment;
import org.example.shape.Rectangle;
import org.example.shape.Rhombus;
import org.example.shape.Shape;
import org.example.shape.Square;
import org.example.visitor.CalcAreaVisitor;
import org.example.visitor.CalcPerimeterVisitor;
import org.example.visitor.DrawingVisitor;

/**
 * Main.
 *
 * @author Tatyana_Dolnikova
 */
public class Main {

    public static void main(String[] args) {
        var rhombus = new Rhombus(7, 5, 3);
        var rectangle = new Rectangle(3, 15);
        var square = new Square(4);
        var lineSegment = new LineSegment(30);

        Shape[] shapes = {rhombus, rectangle, square, lineSegment};

        var calcAreaVisitor = new CalcAreaVisitor();
        var calcPerimeterVisitor = new CalcPerimeterVisitor();
        var drawingVisitor = new DrawingVisitor();

        for (Shape shape : shapes) {
            shape.printName();
            shape.accept(drawingVisitor);
            shape.accept(calcAreaVisitor);
            shape.accept(calcPerimeterVisitor);
            System.out.println();
        }

    }
}
